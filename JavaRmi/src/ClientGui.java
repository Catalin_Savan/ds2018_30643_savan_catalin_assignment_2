import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class ClientGui extends Container {
    private JButton taxButton;
    private JButton priceButton;
    private JTextField textField1;
    private JTextField textField2;
    private JTextField textField3;
    private JTextArea priceOperation;
    public JPanel panelMain;

    public ClientGui() {
        priceButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                try {
                    // Getting the registry
                    Registry registry = LocateRegistry.getRegistry(null);

                    // Looking up the registry for the remote object
                    Hello stub = (Hello) registry.lookup("Hello");

                    // Calling the remote method using the obtained object
                    priceOperation.setText("");
                    priceOperation.append(String.valueOf(stub.sellingPrice(Integer.parseInt(textField1.getText()),Integer.parseInt(textField2.getText()),Double.parseDouble(textField3.getText()))));

                    // System.out.println("Remote method invoked");
                } catch (Exception ev) {
                    System.err.println("Client exception: " + e.toString());
                    ev.printStackTrace();
                }
            }
        });
        taxButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                try {
                    // Getting the registry
                    Registry registry = LocateRegistry.getRegistry(null);

                    // Looking up the registry for the remote object
                    Hello stub = (Hello) registry.lookup("Hello");

                    // Calling the remote method using the obtained object
                    priceOperation.setText("");
                    priceOperation.append(String.valueOf(stub.taxCalculator(Integer.parseInt(textField1.getText()),Integer.parseInt(textField2.getText()),Double.parseDouble(textField3.getText()))));

                    // System.out.println("Remote method invoked");
                } catch (Exception ev) {
                    System.err.println("Client exception: " + e.toString());
                    ev.printStackTrace();
                }
            }
        });
    }
}
