// Implementing the remote interface
public class ImplHello implements Hello {

    // Implementing the interface method
    public void printMsg() {
        System.out.println("This is an example RMI program");
    }

    @Override
    public double taxCalculator(int year, int engineSize, double price) throws RuntimeException {
        double tax =0;

        if ( engineSize < 1600)
        {
            tax = (engineSize/200)*8;
        }
        if ( engineSize > 1600 && engineSize <=2000)
        {
            tax = (engineSize/200)*18;
        }
        if ( engineSize > 2000 && engineSize <=2600)
        {
            tax = (engineSize/200)*72;
        }
        if ( engineSize > 2600 && engineSize <=3000)
        {
            tax = (engineSize/200)*144;
        }
        if ( engineSize > 3000)
        {
            tax = (engineSize/200)*290;
        }

        return tax;
    }

    @Override
    public double sellingPrice(int year, int engineSize, double price) throws RuntimeException {
        double sellingPrice=0;

        if (2018-year<7)
        {
            sellingPrice=price-(price/7)*(2018-year);
        }
        else
        {
            sellingPrice=price;
        }
        return sellingPrice;


    }

}