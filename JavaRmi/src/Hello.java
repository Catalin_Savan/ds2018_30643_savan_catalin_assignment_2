import java.rmi.Remote;
import java.rmi.RemoteException;

// Creating Remote interface for our application 
public interface Hello extends Remote {
    void printMsg() throws RemoteException;

    double taxCalculator(int year, int engineSize, double price) throws RemoteException;

    double sellingPrice(int year, int engineSize, double price) throws RemoteException;
} 